#include <iostream>

using namespace std;

int main()
{
    // Беззнаковый массив байтов
    unsigned short array[] = {43, 111, 8, 189, 233, 94, 22, 1, 55, 210};
    // Счетчик нужных чисел
    int count = 0;
    // Маска, по которой сверяем 
    unsigned short mask = 0x22;

    for (int i = 0; i < 10; i++)
    {
        // Проверяем число по маске
        unsigned short tmp = array[i] & mask;
        // Если совпадает, то увеличиваем счетчик
        if (tmp == 0)
            count++;
    }

    cout << "Count = " << count << endl;

    return 0;
}
