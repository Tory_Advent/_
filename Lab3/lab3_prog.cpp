#include <iostream>

using namespace std;

int main()
{
    srand(time(0));

    // Размер массива
    const int N = 10;
    // Массив
    int array[N];

    // Генерация массива
    cout << "Array: ";
    for (int i = 0; i < N; i++)
    {
        // Рандомное значение от 0 до 99
        array[i] = rand() % 100;
        cout << array [i] << "  ";
    }
    cout << endl;

    // Переменная для итоговой суммы квадратов
    int result;
    // Указатель на перый эл. массива
    int* ptr_arr = &array[0];

    // Ассемблерная вставка, которая считает сумму
    asm(
        // Кидаем параметры в регистры
        "movq %[ptr_arr], %%rsi;"
        "movl %[N], %%ecx;"
        // Обнуляем результат суммы
        "movl $0, %[result];"

    "sum:"
        // Кидаем в eax значение из указателя на массив
        "movl (%%rsi), %%eax;"
        // Перемножаем самого на себя
        "mull %%eax;"
        // Суммируем получ. число с результатом
        "addl %%eax, %[result];"
        // Смещаем указатель дальше (4 из-за int (4 байта))
        "addq $4, %%rsi;"

        // Повторяем это действие ecx раз (N)
        "loop sum;"
        : // Переменные, которые используются во вставке
        : [ptr_arr]"m"(ptr_arr), [N]"m"(N), [result]"m"(result)
        :
    );


    cout << "Result = " << result << endl;

    return 0;
}
