#include "PinNames.h"
#include "ThisThread.h"
#include "mbed.h"
//igitalOut oled1(LED1);
DigitalOut oled1(D7);
// Объявляем пин кнопки 1 как вход прерывания
InterruptIn ibutton1 (BUTTON1);
// Задержка, 1 секунда == 1000 мс
static auto sleep_time = 1000ms;

Ticker toggle_led_ticker;

void led_ticker()
{
    oled1 = !oled1;
}

void pressed()
{
    toggle_led_ticker.detach(); // открепляет таймер
    // Управление скоростью мерцания светодиода
sleep_time += 250ms;
if (sleep_time > 1000ms)
sleep_time = 250ms;
    toggle_led_ticker.attach(&led_ticker, sleep_time); // прикрепляет
}

void released()
{
    oled1=0;
}

// Основной поток main
int main()
{
    toggle_led_ticker.attach(&led_ticker, sleep_time);

    ibutton1.fall(&released);
    ibutton1.rise(&pressed);
    while (true) {
    // основной суперцикл можно использовать для других задач
    }
}



