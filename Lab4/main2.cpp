#include "PinNames.h"
#include "ThisThread.h"
#include "mbed.h"

// Объявляем пин светодиода 1 как выход
DigitalOut oled1(LED1);
InterruptIn ibutton1 (BUTTON1);
// Задержка, 1 секунда == 1000 мс
static auto sleep_time = 1000ms;


Ticker toggle_led_ticker;

void led_ticker()
{
    oled1 = !oled1;
}

void pressed()
{
    //toggle_led_ticker.detach(); // открепляет таймер
    static int count = 0;
    count += 1;
    if (count == 3)
    {
        led_ticker();
        count = 0;
    } 
    //toggle_led_ticker.attach(&led_ticker, sleep_time); // прикрепляет
}



// Основной поток main
int main()
{
    //toggle_led_ticker.attach(&led_ticker, sleep_time);

    ibutton1.rise(&pressed);
    while (true) {
       
}
}

