#include "DigitalOut.h"
#include "PinNames.h"
#include "ThisThread.h"
#include "Thread.h"
#include "mbed.h"

DigitalOut oled1(PA_8);
DigitalOut oled2(LED1);
InterruptIn ibutton1 (BUTTON1);

static auto sleep_time = 1000ms;

typedef struct {
    bool led1_status;
    bool led2_status;
}message_t;

MemoryPool<message_t, 16> mpool;
Queue<message_t, 16> queue;
Thread thread;
static bool status_led;

void send_thread(void)
{
    while (true) 
    {
        message_t *message = mpool.alloc();
        message->led1_status = status_led;
        oled2 = !oled2;
        message->led2_status = oled2;
        queue.put(message);
        ThisThread::sleep_for(1000);
    }
}


void pressed()
{
    status_led = !status_led;
}



// Основной поток main
int main()
{
    //toggle_led_ticker.attach(&led_ticker, sleep_time);
    thread.start(callback(send_thread));
    ibutton1.rise(&pressed);
    while (true) 
    {
        osEvent evt = queue.get();
        if (evt.status == osEventMessage) 
        {
            message_t *message = (message_t *)evt.value.p;
            printf("\nLed 1 Status: %d ", message->led1_status);
            oled1 = message->led1_status;
            printf("Led 2 Status: %d", message->led2_status);
            mpool.free(message);
        }
    }
}

