#include "DigitalOut.h"
#include "PinNames.h"
#include "ThisThread.h"
#include "Thread.h"
#include "mbed.h"
#include "BME280.h"

DigitalOut oled1(LED1);
InterruptIn ibutton1 (BUTTON1);
static int blink_f;

BME280 sensor(I2C_SDA, I2C_SCL);

void led_blink(int blink_param)
{
    if (blink_param <=40)
        oled1 = 1;
    else
    {
        oled1 = !oled1;
    }
}


// Основной поток main
int main()
{
    while (true) 
    {
        printf("%d degC, %d hPa, %d %%\n", (int)sensor.getTemperature(), (int)sensor.getPressure(), (int)sensor.getHumidity());
        blink_f = (int)sensor.getHumidity();
        led_blink(blink_f);
        ThisThread::sleep_for(1000ms);
    }
}

